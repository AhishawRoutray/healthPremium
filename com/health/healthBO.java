package com.health;

public class healthBO {
	String name;
	String gender;
	int age;
	String HyperTension;
	String BloodPressure;
	String BloodSugar;
	String Overweight;
	String Smoking;
	String Alcohol;
	String DailyExercise;
	String Drugs;
	
	//getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHyperTension() {
		return HyperTension;
	}
	public void setHyperTension(String hyperTension) {
		HyperTension = hyperTension;
	}
	public String getBloodPressure() {
		return BloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		BloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return BloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		BloodSugar = bloodSugar;
	}
	public String getOverweight() {
		return Overweight;
	}
	public void setOverweight(String overweight) {
		Overweight = overweight;
	}
	public String getSmoking() {
		return Smoking;
	}
	public void setSmoking(String smoking) {
		Smoking = smoking;
	}
	public String getAlcohol() {
		return Alcohol;
	}
	public void setAlcohol(String alcohol) {
		Alcohol = alcohol;
	}
	public String getDailyExercise() {
		return DailyExercise;
	}
	public void setDailyExercise(String dailyExercise) {
		DailyExercise = dailyExercise;
	}
	public String getDrugs() {
		return Drugs;
	}
	public void setDrugs(String drugs) {
		Drugs = drugs;
	}
	
	
}
